import React from "react";
import { Provider as PaperProvider } from "react-native-paper";
import { Provider as StoreProvider } from "react-redux";
import StackNavigator from "./navigation/index";
import store from "./redux/store/store";

export default function app() {
  return (
    <StoreProvider store={store}>
      <PaperProvider>
        <StackNavigator></StackNavigator>
      </PaperProvider>
    </StoreProvider>
  );
}
