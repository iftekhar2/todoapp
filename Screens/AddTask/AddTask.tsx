import React, { useState } from "react";
import { StyleSheet, TextInput, View } from "react-native";
import { FAB, IconButton } from "react-native-paper";
import Header from "../../components/Header/Header";

const createRandomInt = (min: number, max: number) =>
  Math.floor(Math.random() * (max + 1 - min)) + min;

const AddTask = ({ navigation }: { navigation: any }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const onSaveNote = () => {
    let dateInit = new Date();
    let date = dateInit.toDateString();
    navigation.state.params.addTask({
      title,
      description,
      id: createRandomInt(0, 1000000),
      date: date,
    });
    navigation.goBack();
  };

  return (
    <>
      <Header titleText="Add a New Task" />
      <IconButton
        icon="close"
        size={25}
        color="white"
        onPress={() => navigation.goBack()}
        style={styles.iconButton}
      />
      <View style={styles.container}>
        <TextInput
          placeholder="Note Title"
          value={title}
          onChangeText={setTitle}
          style={styles.title}
        />
        <TextInput
          placeholder="Note Description"
          value={description}
          onChangeText={setDescription}
          style={styles.text}
          multiline={true}
          scrollEnabled={true}
          returnKeyLabel="done"
          blurOnSubmit={true}
        />
        <FAB
          style={styles.fab}
          small
          icon="check"
          disabled={title === "" ? true : false}
          onPress={() => onSaveNote()}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  iconButton: {
    backgroundColor: "black",
    position: "absolute",
    right: 0,
    top: 20,
    // margin: 10,
  },
  titleContainer: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  title: {
    fontSize: 24,
    marginBottom: 16,
  },
  text: {
    height: 300,
    fontSize: 16,
  },
  fab: {
    position: "absolute",
    margin: 20,
    right: 0,
    bottom: 0,
  },
});

export default AddTask;
