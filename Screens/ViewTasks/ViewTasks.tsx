import React from "react";
import { Button, FlatList, StyleSheet, Text, View } from "react-native";
// import { FAB } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import Header from "../../components/Header/Header";
import TaskItem from "../../components/TaskItem/TaskItem";
import {
  add,
  deletion,
  loadTasks,
  saveTasks,
} from "../../redux/reducer/tasksApp";

const ViewTasks = ({ navigation }: { navigation: any }) => {
  /////////////////////////////Accessing Redux Action Dispatch////////////////////////
  const tasks: any = useSelector<any>((state) => state);
  const dispatch = useDispatch();

  interface Task {
    title: string;
    description: string;
    id: number;
    date: string;
  }

  const addTask = (task: Task) => {
    dispatch(add(task));
    saveTasks(task);
  };

  const deleteTask = (id: number) => {
    dispatch(deletion(id));
  };

  // const onSave = () => {
  //   dispatch(saveTasks());
  //   console.log("saved");
  // };

  const onLoads = () => {
    dispatch(loadTasks());
    console.log("Load");
  };

  /////////////////////////////////////////////////////////////////////////////////////////

  const keyExtractor = (item: any) => {
    // console.log("keyExtractor", item);
    return item.task.id.toString();
  };

  return (
    <>
      <Header titleText="To Do App" />
      <View style={styles.container}>
        {tasks.length === 0 ? (
          <View style={styles.titleContainer}>
            <Text style={styles.title}>No Tasks To Be Listed Yet</Text>
          </View>
        ) : (
          <FlatList
            data={tasks}
            renderItem={({ item }) => (
              <TaskItem
                name={item.task.title}
                desc={item.task.description}
                // getting the id entity
                id={item.task.id}
              ></TaskItem>
            )}
            keyExtractor={keyExtractor}
          />
        )}

        <View style={styles.button}>
          <Button title="Save" onPress={() => console.log("test")} />
          <Button
            color="green"
            title="Add"
            onPress={() => navigation.navigate("AddTask", { addTask })}
          />
          <Button title="Load" onPress={onLoads} />
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  titleContainer: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  title: {
    fontSize: 20,
  },
  button: {
    // backgroundColor: "black",
    // marginTop: 30,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  listTitle: {
    fontSize: 20,
  },
});

export default ViewTasks;
