import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import AddTask from "../Screens/AddTask/AddTask";
import ViewTasks from "../Screens/ViewTasks/ViewTasks";

const StackNavigator = createStackNavigator(
  {
    ViewTasks: {
      screen: ViewTasks,
    },
    AddTask: {
      screen: AddTask,
    },
  },
  {
    initialRouteParams: ViewTasks,
    headerMode: "none",
    mode: "card",
  }
);

export default createAppContainer(StackNavigator);
