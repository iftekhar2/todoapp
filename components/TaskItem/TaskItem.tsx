import { EvilIcons } from "@expo/vector-icons";
import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { add, deletion } from "../../redux/reducer/tasksApp";

const TaskItem = (props: any) => {
  /////////////////////////////Accessing Redux Action Dispatch////////////////////////
  const tasks: any = useSelector<any>((state) => state);
  const dispatch = useDispatch();

  // console.log("current tasks:", tasks);

  const addTask = (task: any) => {
    dispatch(add(task));
  };

  const deleteTask = (id: number) => {
    // console.log("Deleted", tasks);
    dispatch(deletion(id));
    // console.log("deleted");
    // console.log(tasks);
  };

  //////////////////////test functions///////////////////////////////////////
  const handleTaskPress = () => {
    console.log("Task Pressed");
  };
  // const deleteTaskPress = () => {
  //   console.log("Deleted");
  // };
  //////////////////////////////////////////////////////////////

  return (
    <View style={styles.master}>
      <TouchableOpacity
        onPress={() => handleTaskPress()}
        style={styles.leftBox}
      >
        <Text>{props.name}</Text>
        <Text>{props.desc}</Text>
        <Text>{props.id}</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => deleteTask(props.id)}
        style={styles.rightBox}
      >
        <EvilIcons name="trash" size={44} color="black" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  rightBox: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30,
    marginLeft: 20,
    height: 70,
    padding: 5,
  },
  leftBox: {
    width: "70%",
    height: "80%",
    borderColor: "black",
    borderRadius: 10,
    // borderWidth: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
    paddingLeft: 10,
    flexWrap: "wrap",
    marginTop: 6,
  },
  master: {
    height: 150,
    width: "90%",
    marginTop: 40,
    marginLeft: "5%",
    marginRight: "5%",
    borderColor: "black",
    borderRadius: 10,
    borderWidth: 1,
    paddingLeft: 10,
    paddingTop: 10,
    // alignContent: "center",
    // alignItems: "center",
    // justifyContent: "center",
    flexDirection: "row",
  },
});

export default TaskItem;

// import { AntDesign } from "@expo/vector-icons";
// import React from "react";
// import { StyleSheet, Text, View } from "react-native";

// const TaskItem = (props: any) => {
//   return (
//     <View style={styles.itemBox}>
//       <View style={styles.item}>
//         <View style={styles.itemDisplay}>
//           <View>
//             <AntDesign name="tags" size={24} color="black" />
//           </View>
//           <Text style={styles.itemText}>{props.text}</Text>
//         </View>
//         <Text style={styles.deleteButton}>-</Text>
//       </View>
//       <View style={styles.taskDetail}>
//         <Text>Test</Text>
//       </View>
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   itemBox: {
//     height: 100,
//     backgroundColor: "white",
//     borderRadius: 10,
//     marginBottom: 20,
//     shadowColor: "#000",
//     shadowOffset: {
//       width: 0,
//       height: 10,
//     },
//     shadowOpacity: 0.51,
//     shadowRadius: 13.16,
//     elevation: 20,
//   },
//   taskDetail: {
//     paddingLeft: 50,
//   },
//   item: {
//     padding: 20,
//     flexDirection: "row",
//     alignItems: "center",
//     alignContent: "center",
//     justifyContent: "space-between",
//   },
//   itemDisplay: {
//     flexDirection: "row",
//     alignItems: "center",
//     flexWrap: "wrap",
//     justifyContent: "space-between",
//   },
//   itemText: {
//     width: "80%",
//   },
//   deleteButton: {
//     width: 30,
//     height: 30,
//     backgroundColor: "white",
//     borderRadius: 60,
//     justifyContent: "center",
//     alignItems: "center",
//     // borderRadius: 60,
//     borderWidth: 1,
//     borderColor: "black",
//   },
// });

// export default TaskItem;
