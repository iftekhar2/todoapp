export const ADD_TASK = "ADD_TASK";
export const DELETE_TASK = "DELETE_TASK";
export const SET_TASKS = "SET_TASKS";

interface Task {
  title: string;
  description: string;
  id: number;
  date: string;
}

// const taskState: any = useSelector<any>((state) => state);

export const add = (task: Task) => {
  return {
    type: ADD_TASK,
    task,
  };
};

export const deletion = (id: number) => {
  return {
    type: DELETE_TASK,
    payload: id,
  };
};

export const setNotes = (tasks: Task) => ({
  type: SET_TASKS,
  payload: tasks,
});

const initialState: Array<{ task: Task }> = [];

const tasksReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case ADD_TASK:
      return [...state, { task: action.task }];
    case DELETE_TASK:
      const deleteATask = state.filter((t) => t.task.id !== action.payload);
      return deleteATask;
    case SET_TASKS:
      // to have the unique tasks in the updated state
      const uniqueArray: Array<{ task: Task }> = [];
      const newTasks = action.payload.map((task) => ({ task }));
      newTasks.forEach(({ task }) => {
        const newId = task.id;
        const sameIdElement = state.filter((t) => t.task.id === newId);
        if (sameIdElement.length === 0) {
          uniqueArray.push({ task });
        } else {
          uniqueArray.push(sameIdElement[0]);
        }
      });
      return uniqueArray;
    default:
      return state;
  }
};

export const saveTasks = (task) => {
  console.log(task);
  console.log("saved");
  fetch(
    "http://ec2-13-115-61-236.ap-northeast-1.compute.amazonaws.com:3000/tasks",
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
      body: JSON.stringify(task),
    }
  );
};

export const loadTasks = () => async (dispatch: any, getState: any) => {
  const notes = await fetch(
    "http://ec2-13-115-61-236.ap-northeast-1.compute.amazonaws.com:3000/tasks?page=1&limit=5"
  ).then((res) => res.json());
  //console.log(notes);

  const converted = notes.items.map((task: any) => ({
    title: task.title,
    description: task.description,
    id: task.id,
  }));
  dispatch(setNotes(converted));
};

export default tasksReducer;
