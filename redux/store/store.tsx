import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import tasksReducer from "../reducer/tasksApp";

const store = createStore(tasksReducer, applyMiddleware(thunk));

export default store;
